### Steps to reproduce

1.
2.
3.

### Expected behaviour

Tell us what should happen

### Actual behaviour

Tell us what happens instead

### Server configuration

**Operating system**:

**Web server:**

**Database:**

**Virtual Universe version:**

### Logs

Please make sure that Virtual Universe has made an log-file and include a link to it here
