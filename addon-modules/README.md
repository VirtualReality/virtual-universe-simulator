# Optional-Modules
==========================

In this folder you can drop the modules from the Optional Repository that you want to use with Virtual Universe

Please remember to drop the full folder of the module you want to include as it holds the prebuild.xml configuration information that will be read when uou use the runprebuild.* script for your system.
