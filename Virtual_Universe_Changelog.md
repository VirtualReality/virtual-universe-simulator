#Virtual Universe Changelog

The following are the changes made in each version of Virtual Universe

# Version: 1.0.2 RC1

- Styling cleanup for console messages (The remainder will be included in RC2)
- Fixes to the World Map for regions bigger then 1024x1024 not showing correctly in the viewers
- BaseCurrency fully upgraded and now is functional (this is without the payment processor for purchasing in-world currency and regions)
- Cleanup of outdated code (There will always be more code cleanup in later versions)
- Module cleanup and upgrades
- Fixes for terrain including commands for terrain-load-tile
- Multiple attachments temporarily turned off by default while we investigate why adding multiple attachments to avatars does not work properly
- Correct the default owner of an abandoned parcel when a region changes estates (This includes corrections in the auctions of parcels)
- General code upgrades